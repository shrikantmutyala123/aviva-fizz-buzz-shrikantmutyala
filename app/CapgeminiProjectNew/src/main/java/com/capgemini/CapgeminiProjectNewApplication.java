package com.capgemini;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CapgeminiProjectNewApplication {

	public static void main(String[] args) {
		SpringApplication.run(CapgeminiProjectNewApplication.class, args);
	}
}
