package com.capgemini.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.service.impl.BasicFizzBuzzService;

@RestController
@RequestMapping("/api")
public class BasicFizzBuzzRestController {

	@Value("${error.message1}")
	private String err_1;

	@Value("${error.message2}")
	private String err_2;

	@Autowired
	private BasicFizzBuzzService basicFizzBuzzService;

	@RequestMapping(value = "/basicFizzBuzz/{anyNumber}", method = RequestMethod.GET)
	public ResponseEntity<?> basicFizzBuzz(@PathVariable("anyNumber") Integer anyNumber) {
		if (anyNumber == null) {
			return new ResponseEntity<>(new String(err_1), HttpStatus.BAD_REQUEST);
		}
		if (anyNumber >= 1 && anyNumber <= 1000) {
			// continue
		} else {
			return new ResponseEntity<>(new String(err_2), HttpStatus.BAD_REQUEST);
		}
		ArrayList<Object> response = basicFizzBuzzService.getResponse(anyNumber);
		return new ResponseEntity<>(response, HttpStatus.OK);

	}

	@RequestMapping(value = "/basicFizzBuzzWithPagination/{anyNumber}", method = RequestMethod.GET)
	public ResponseEntity<?> basicFizzBuzzWithPagination(@PathVariable("anyNumber") Integer anyNumber,
			@RequestParam("next") Integer next, @RequestParam("previous") Integer previous) {

		if (anyNumber == null) {
			return new ResponseEntity<>(new String(err_1), HttpStatus.BAD_REQUEST);
		}
		if (anyNumber >= 1 && anyNumber <= 1000) {
			// continue
		} else {
			return new ResponseEntity<>(new String(err_2), HttpStatus.BAD_REQUEST);
		}
		ArrayList<Object> response = basicFizzBuzzService.getResponse(anyNumber);
		double sizeOfList =(double) response.size();
		int numberOfPages = (int) Math.ceil(sizeOfList / 20);
		if (previous != 0) {
			previous = 1;
		}

		if (response.size() > 20) {
			if (! (next <= numberOfPages)) {
				return new ResponseEntity<>("Page size must be less than or equals to " + numberOfPages, HttpStatus.OK);
			}

			int value = next - previous;
			int value2 = value * 20;

			if (value2 > sizeOfList) {
				value2 = (int) sizeOfList;
			}

			List<Object> subList = response.subList(previous * 20, value2);
			return new ResponseEntity<>(subList, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(response, HttpStatus.OK);
		}

	}

}
