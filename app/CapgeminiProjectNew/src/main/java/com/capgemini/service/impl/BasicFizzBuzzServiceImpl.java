package com.capgemini.service.impl;

import java.time.DayOfWeek;
import java.time.ZonedDateTime;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service("basicFizzBuzzService")
public class BasicFizzBuzzServiceImpl implements BasicFizzBuzzService {

	@Override
	public ArrayList<Object> getResponse(Integer anyNumber) {

		ArrayList<Object> response = new ArrayList<>();
		
		for (int value = 1; value <= anyNumber; value++) {
			boolean status1 = false;
			boolean status2 = false;
			boolean status3 = false;
			if ((value % 3) == 0) {
				if (isWEDNESDAY()) {
					response.add("wizz");
				} else {
					response.add("fizz");
				}
				status1 = true;
			}
			if ((value % 5) == 0) {
				// print buzz
				if (isWEDNESDAY()) {
					response.add("wuzz");
				} else {
					response.add("buzz");
				}
				status2 = true;
			}

			if ((value % 5) == 0 && (value % 3 )== 0) {
				// print buzz
				if (isWEDNESDAY()) {
					response.add("wizz and wuzz");
				} else {
					response.add("fizz and buzz");
				}
				status3 = true;
			}

			if (status1 == false && status2 == false && status3 == false) {
				response.add(value);
			}

		}

		return response;
	}

	public boolean isWEDNESDAY() {
		ZonedDateTime now = ZonedDateTime.now();
		DayOfWeek dayOfWeek = now.getDayOfWeek();
		if (dayOfWeek.equals(DayOfWeek.WEDNESDAY)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isWEDNESDAY2() {
		ZonedDateTime now = ZonedDateTime.now();
		DayOfWeek dayOfWeek = now.getDayOfWeek();
		if (dayOfWeek.equals(DayOfWeek.WEDNESDAY)) {
			return true;
		} else {
			return false;
		}
	}
	
//	public static void main(String[] args) {
//		int anyNumber=20;
//		ArrayList<Object> response = new ArrayList<>();
//		
//		for (int value = 1; value <= anyNumber; value++) {
//			boolean status1 = false;
//			boolean status2 = false;
//			boolean status3 = false;
//			if ((value % 3) == 0) {
//				if (isWEDNESDAY2()) {
//					response.add("wizz");
//				} else {
//					response.add("fizz");
//				}
//				status1 = true;
//			}
//			if ((value % 5) == 0) {
//				// print buzz
//				if (isWEDNESDAY2()) {
//					response.add("wuzz");
//				} else {
//					response.add("buzz");
//				}
//				status2 = true;
//			}
//
//			if ((value % 5) == 0 && (value % 3 )== 0) {
//				// print buzz
//				if (isWEDNESDAY2()) {
//					response.add("wizz and wuzz");
//				} else {
//					response.add("fizz and buzz");
//				}
//				status3 = true;
//			}
//
//			if (status1 == false && status2 == false && status3 == false) {
//				response.add(value);
//			}
//
//		}
//	}

	

}
