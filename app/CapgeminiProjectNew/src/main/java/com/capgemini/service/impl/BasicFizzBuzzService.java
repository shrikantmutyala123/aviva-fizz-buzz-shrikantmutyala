package com.capgemini.service.impl;

import java.util.ArrayList;

public interface BasicFizzBuzzService {

	ArrayList<Object> getResponse(Integer anyNumber);

}
