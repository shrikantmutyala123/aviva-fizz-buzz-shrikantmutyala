package com.capgemini;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.capgemini.controller.BasicFizzBuzzRestController;
import com.capgemini.service.impl.BasicFizzBuzzService;

@RunWith(SpringRunner.class)
@WebMvcTest(BasicFizzBuzzRestController.class)
public class CapgeminiProjectNewApplicationTests {

	@Test
	public void contextLoads() {
	}

	@MockBean
	private BasicFizzBuzzService basicFizzBuzzService;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void testbasicFizzBuzz() throws Exception {
		mockMvc.perform(get("/api/basicFizzBuzz/20")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"));

	}

	@Test
	public void testbasicFizzBuzzForBadRequest() throws Exception {
		mockMvc.perform(get("/api/basicFizzBuzz/0")).andExpect(status().isBadRequest());

	}
	
	@Test
	public void testbasicFizzBuzzWithPagination() throws Exception {
		mockMvc.perform(get("/api/basicFizzBuzzWithPagination/50?next=2&previous=0")).andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"));

	}

	@Test
	public void testbasicFizzBuzzWithPaginationForBadRequest() throws Exception {
		mockMvc.perform(get("/api/basicFizzBuzzWithPagination/0?next=2&previous=0")).andExpect(status().isBadRequest());

	}

}
